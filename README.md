# Make Us Your Default Search Engine

Repository that displays how to set us as your default search engine in different browsers

## Firefox/Cliqz

#### Option 1: 
Go to [https://addons.mozilla.org/addon/add-infinity-search/](https://addons.mozilla.org/addon/add-infinity-search/) and
then click the Add to Firefox button. 

#### Option 2: 
For Firefox, go to [https://infinitysearch.co/](https://infinitysearch.co) and then click on the green plus
 on the search bar (shown in the image below) and then click on Add "Infinity Search".
![Firefox](screenshots/Firefox.png)

Doing this only adds Infinity Search to the list of search engines available in Firefox.

To make us your default search engine, go to [about:preferences#search](about:preferences#search)
and change the default search engine to Infinity Search (as shown in the image below). 

![Firefox](screenshots/Firefox2.png)


## Brave/Chrome
1. Go to settings
2. Go to search engines
3. Press manage search engines 
4. Press add
5. Add the information show below:
![Brave](screenshots/Brave.png)
The search engine and keyword value can be set to anything but the 
URL has to be https://infinitysearch.co/results?q=%s

## Vivaldi 
1. Go to settings 
2. Go to search 
3. Press the + button 
5. Add the information show below:
![Vivaldi](screenshots/Vivaldi.png)
The search engine and nickname value can be set to anything but the 
URL has to be https://infinitysearch.co/results?q=%s

